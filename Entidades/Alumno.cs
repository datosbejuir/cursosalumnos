﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Alumno: IEquatable<Alumno>

    {
        private int _NumeroMatricula;
        public int NumeroMatricula;

        public Alumno(int numMatricula, string dni, string nombre, string curso, string año)
        {
            this.NumeroMatricula = numMatricula;
            this.Dni = dni;
            this.Nombre = nombre;
            this.Curso = curso;
            this.Año = año;
        }

        public Alumno()
        {

        }

        public int nMatricula
        {
            get
            {
                return _NumeroMatricula;
            }

            set
            {
                _NumeroMatricula = value;
            }
        }

        public String Dni
        {
            get; set;
        }

        public String Nombre
        {
            get; set;
        }

        public String Curso
        {
            get; set;
        }

        public String Año
        {
            get; set;
        }

        public bool Equals(Alumno other)
        {
            return (other != null) && (this.nMatricula == other.nMatricula);
            //if (other == null) return false;
            //if (this.nMatricula == other.nMatricula) return true;
            //else return false;
        }
    }
}
