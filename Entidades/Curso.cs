﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Curso: IEquatable<Curso>
    {
        public Curso(String id, string nombre)
        {
            this.Id = id;
            this.Nombre = nombre;
        }

        public Curso()
        {

        }
        public String Id
        {
            get;
            set;
        }

        public String Nombre
        {
            get;
            set;
        }

        public bool Equals(Curso other)
        {
            return (other != null) && (this.Id == other.Id);
        }
    }
}
