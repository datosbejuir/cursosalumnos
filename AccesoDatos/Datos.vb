﻿Imports System.IO
Imports Entidades
Public Class Datos
    ''ruta del directorio donde guardaremos los archivo
    Private ruta As String = My.Application.Info.DirectoryPath
    ''Lista de alumnos

    ''Lista de cursos
    Private _Cursos As New List(Of Entidades.Curso)
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns>Datos de los alumnos</returns>
    Public Property Alumnos As ObjectModel.Collection(Of Alumno)

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns>Datos de los cursos</returns>
    Public ReadOnly Property Cursos As ObjectModel.ReadOnlyCollection(Of Entidades.Curso)
        Get
            Return _Cursos.AsReadOnly
        End Get
    End Property
    ''' <summary>
    ''' 
    ''' </summary>
    Public Sub New()
        ''Se supone que siempre existirá el fichero de cursos
        If File.Exists(ruta & "\Cursos.txt") Then
            Dim ficheroCursos As StreamReader = Nothing
            Try
                ficheroCursos = New StreamReader(ruta & "\Cursos.txt")
                ''llenamos la colección de cursos
                Do Until ficheroCursos.EndOfStream
                    Dim linea As String = ficheroCursos.ReadLine
                    Dim datos() As String = linea.Split("*")
                    _Cursos.Add(New Curso(datos(0), datos(1)))
                    ''Id, nombreCurso
                Loop
            Catch ex As Exception
            Finally
                ficheroCursos.Close()
            End Try
        End If
        ''Lo mismo ccon alumnos
        If File.Exists(ruta & "\Alumnos.txt") Then
            Dim ficheroAlumnos As StreamReader = Nothing
            Try
                ficheroAlumnos = New StreamReader(ruta & "\Alumnos.txt")
                Do Until ficheroAlumnos.EndOfStream
                    Dim linea As String = ficheroAlumnos.ReadLine
                    Dim datos() As String = linea.Split("*")
                    _Alumnos.Add(New Alumno(Integer.Parse(datos(0)), datos(1), datos(2), datos(3), datos(4)))
                    ''NºMatricula(int), dni, nombre entero, idCurso, añoAcademico
                Loop
            Catch ex As Exception
            Finally
                ficheroAlumnos.Close()
            End Try
        End If

    End Sub

    Public Function Grabar(ByVal listaAlumnos As List(Of Alumno)) As String

        Dim ficheroAlumnos As StreamWriter = Nothing
        Try
            ficheroAlumnos = New StreamWriter(ruta & "\Alumnos.txt")
            For Each al In listaAlumnos
                ficheroAlumnos.WriteLine(al.nMatricula & "*" & al.Dni & "*" & al.Nombre & "*" & al.Curso & "*" & al.Año)
            Next
        Catch ex As Exception
            ficheroAlumnos.Close()
        Finally
            ficheroAlumnos.Close()
        End Try
        Return ""
    End Function
End Class
