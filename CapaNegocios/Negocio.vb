﻿Imports Entidades, AccesoDatos
Public Class Negocio

    Private misDatos As New Datos
    Public alumnos As New List(Of Alumno)
    Public cursos As New List(Of Curso)

    ''' <summary>
    ''' Constructor donde igualamos la lista de alumnos de la capa Datos a la lista de alumnos de la capa Negocio
    ''' </summary>
    Public Sub New()
        If misDatos.Alumnos IsNot Nothing Then alumnos = misDatos.Alumnos.ToList ''Puede que en el archivo no tengamos alumnos, entonces daría error a no ser que pongamos controlemos los nulos
        cursos = misDatos.Cursos.ToList
        ''Como es una lista de solo lectura tenemos que poner el .ToList
    End Sub

    ''' <summary>
    ''' Función privada para añadir alumnos a la lista de alumnos de Negocio
    ''' </summary>
    ''' <param name="nMatricula"></param>
    ''' <param name="dni"></param>
    ''' <param name="nombre"></param>
    ''' <param name="curso"></param>
    ''' <param name="año"></param>
    ''' <returns></returns>
    Private Function AñadirAlumnos(ByVal nMatricula As Integer, ByVal dni As String, ByVal nombre As String, ByVal curso As String, ByVal año As String) As String

        Dim alum As New Alumno(nMatricula, dni, nombre, curso, año)
        If TryCast(alum, Alumno).Dni.ToString.Length <> 9 Then Return "Longitud DNI no válida"

        alumnos.Add(alum)

        Return ""
    End Function

    ''' <summary>
    ''' Función publica que devuelve el mensaje si ha sido añadido el alumno
    ''' </summary>
    ''' <param name="nMatricula"></param>
    ''' <param name="dni"></param>
    ''' <param name="nombre"></param>
    ''' <param name="curso"></param>
    ''' <param name="año"></param>
    ''' <returns></returns>
    Public Function Añadir(ByVal nMatricula As Integer, ByVal dni As String, ByVal nombre As String, ByVal curso As String, ByVal año As String) As String

        Dim men As String = AñadirAlumnos(nMatricula, dni, nombre, curso, año)

        Return men

    End Function

    ''' <summary>
    ''' Función privada para borrar alumnos a la lista de alumnos de Negocio
    ''' </summary>
    ''' <param name="nMatricula"></param>
    ''' <returns></returns>
    Private Function BorrarAlumnos(ByVal nMatricula As Integer) As String

        Dim alum As New Alumno(nMatricula, "12312312S", "aaaaaaa", "DAM", "2015/2016")
        If Not alumnos.Contains(alum) Then Return "No existe el alumno"
        alumnos.Remove(alum)
        Return ""
    End Function

    ''' <summary>
    ''' Función publica que devuelve el mensaje si ha sido borrado el alumno
    ''' </summary>
    ''' <param name="nMatricula"></param>
    ''' <returns></returns>
    Public Function Borrar(ByVal nMatricula As Integer) As String

        Dim men As String = BorrarAlumnos(nMatricula)

        Return men

    End Function

    ''' <summary>
    ''' Función pública para buscar alumnos por grado
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    Public Function AlumnosGrado(ByVal id As String) As List(Of Alumno)

        Dim alumnosPorGrado As New List(Of Alumno)

        For Each alumno In alumnos
            If alumno.Curso.Contains(id) Then
                alumnosPorGrado.Add(alumno)
            End If
        Next

        Return alumnosPorGrado
    End Function
    ''' <summary>
    ''' Función pública para buscar alumnos por dni
    ''' </summary>
    ''' <param name="dni"></param>
    ''' <returns></returns>
    Public Function AlumnosDni(ByVal dni As String) As List(Of Alumno)
        ''devuelve varios registros de un mismo dni, colección  de solo lecturatgr

        Dim alumnosPorDni As New List(Of Alumno)

        For Each alumno In alumnos
            If alumno.Dni.Contains(dni) Then
                alumnosPorDni.Add(alumno)
            End If
        Next

        Return alumnosPorDni
    End Function

    ''' <summary>
    ''' Función pública para buscar alumnos por matrícula
    ''' </summary>
    ''' <param name="nMatricula"></param>
    ''' <returns></returns>
    Public Function AlumnoMatricula(ByVal nMatricula As Integer) As List(Of Alumno)

        Dim alumnosPorMatricula As New List(Of Alumno)

        For Each alumno In alumnos
            If alumno.nMatricula = nMatricula Then
                alumnosPorMatricula.Add(alumno)
            End If
        Next

        Return alumnosPorMatricula
    End Function

    ''' <summary>
    ''' Función Grabar que llama a la función Grabar de la capa datos
    ''' </summary>
    ''' <returns></returns>
    Public Function Grabar() As String

        Dim men As String = misDatos.Grabar(alumnos)

        Return men
    End Function
End Class
