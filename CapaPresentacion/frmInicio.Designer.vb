﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInicio
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.AlumnoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AñadirAlumnoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BorrarAlumnoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VerAlumnoDNIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VerAlumnoMATRICULAToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CursosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VerCursosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AlumnoToolStripMenuItem, Me.CursosToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(284, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "CursoAlumno"
        '
        'AlumnoToolStripMenuItem
        '
        Me.AlumnoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AñadirAlumnoToolStripMenuItem, Me.BorrarAlumnoToolStripMenuItem, Me.VerAlumnoDNIToolStripMenuItem, Me.VerAlumnoMATRICULAToolStripMenuItem})
        Me.AlumnoToolStripMenuItem.Name = "AlumnoToolStripMenuItem"
        Me.AlumnoToolStripMenuItem.Size = New System.Drawing.Size(62, 20)
        Me.AlumnoToolStripMenuItem.Text = "&Alumno"
        '
        'AñadirAlumnoToolStripMenuItem
        '
        Me.AñadirAlumnoToolStripMenuItem.Name = "AñadirAlumnoToolStripMenuItem"
        Me.AñadirAlumnoToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.AñadirAlumnoToolStripMenuItem.Text = "&Añadir alumno"
        '
        'BorrarAlumnoToolStripMenuItem
        '
        Me.BorrarAlumnoToolStripMenuItem.Name = "BorrarAlumnoToolStripMenuItem"
        Me.BorrarAlumnoToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.BorrarAlumnoToolStripMenuItem.Text = "&Borrar alumno"
        '
        'VerAlumnoDNIToolStripMenuItem
        '
        Me.VerAlumnoDNIToolStripMenuItem.Name = "VerAlumnoDNIToolStripMenuItem"
        Me.VerAlumnoDNIToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.VerAlumnoDNIToolStripMenuItem.Text = "&Ver alumno DNI"
        '
        'VerAlumnoMATRICULAToolStripMenuItem
        '
        Me.VerAlumnoMATRICULAToolStripMenuItem.Name = "VerAlumnoMATRICULAToolStripMenuItem"
        Me.VerAlumnoMATRICULAToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.VerAlumnoMATRICULAToolStripMenuItem.Text = "&Ver alumno MATRICULA"
        '
        'CursosToolStripMenuItem
        '
        Me.CursosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VerCursosToolStripMenuItem})
        Me.CursosToolStripMenuItem.Name = "CursosToolStripMenuItem"
        Me.CursosToolStripMenuItem.Size = New System.Drawing.Size(55, 20)
        Me.CursosToolStripMenuItem.Text = "&Cursos"
        '
        'VerCursosToolStripMenuItem
        '
        Me.VerCursosToolStripMenuItem.Name = "VerCursosToolStripMenuItem"
        Me.VerCursosToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.VerCursosToolStripMenuItem.Text = "&Ver cursos"
        '
        'frmInicio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmInicio"
        Me.Text = "Curso Academico"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents AlumnoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AñadirAlumnoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BorrarAlumnoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents VerAlumnoDNIToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents VerAlumnoMATRICULAToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CursosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents VerCursosToolStripMenuItem As ToolStripMenuItem
End Class
