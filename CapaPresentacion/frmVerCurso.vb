﻿Imports Entidades, CapaNegocios

Public Class frmVerCurso
    Private Sub frmVerCurso_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If misDatos.cursos.Count > 0 Then
            gbCursos.Visible = True
            lblId.Visible = True
            lblNombre.Visible = True
            Dim yPoint As Integer = 35
            For i As Byte = 0 To misDatos.cursos.Count - 1
                Dim lblId As New Label
                lblId.Name = "Id"
                lblId.Text = misDatos.cursos(i).Id
                lblId.Size = New Size(30, 13)
                lblId.Location = New Point(30, yPoint)
                Me.gbCursos.Controls.Add(lblId)

                Dim lblName As New Label
                lblName.Name = "Nombre"
                lblName.Text = misDatos.cursos(i).Nombre
                lblName.Size = New Size(250, 13)
                lblName.Location = New Point(90, yPoint)
                Me.gbCursos.Controls.Add(lblName)

                yPoint = +15
            Next
        Else
            MessageBox.Show("No hay cursos", "Ver cursos")
        End If
    End Sub


End Class