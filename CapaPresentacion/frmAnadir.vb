﻿Imports CapaNegocios
Public Class frmAnadir

    Dim contadorMatricula As Integer = misDatos.alumnos.Count + 1
    Private Sub cargarCombo()
        cboCurso.Items.Clear()
        For i As Integer = 0 To misDatos.cursos.Count - 1
            cboCurso.Items.Add(misDatos.cursos(i).Nombre)
        Next
    End Sub
    Private Sub limpiarForm()
        txtApellidos.Text = ""
        txtNombre.Text = ""
        txtDNI.Text = ""
        lblMatricula.Text = contadorMatricula + 1
    End Sub

    Private Sub frmAnadir_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim actual As Integer = Today.Year
        lblAño.Text = actual & "/" & (actual + 1)
        lblMatricula.Text = contadorMatricula
        cargarCombo()
    End Sub


    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Me.ErrorProvider1.Clear()
        If txtNombre.Text = "" Then
            Me.ErrorProvider1.SetError(txtNombre, "Introduzca un nombre")
        Else
            Me.ErrorProvider1.SetError(txtNombre, "")
        End If
        If txtApellidos.Text = "" Then
            Me.ErrorProvider1.SetError(txtApellidos, "Introduzca apellidos")
        Else
            Me.ErrorProvider1.SetError(txtApellidos, "")
        End If
        If txtDNI.Text = "" OrElse txtDNI.Text.Length <> 9 Then
            Me.ErrorProvider1.SetError(txtDNI, "Introduzca DNI")
        Else
            Me.ErrorProvider1.SetError(txtDNI, "")
        End If
        If cboCurso.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(cboCurso, "Selecciona un curso")
        Else
            Me.ErrorProvider1.SetError(cboCurso, "")
        End If


        If txtNombre.Text <> "" AndAlso txtApellidos.Text <> "" AndAlso cboCurso.SelectedIndex <> 0 AndAlso txtDNI.Text <> "" Then
            Dim sms As String = misDatos.Añadir(lblMatricula.Text, txtDNI.Text, txtNombre.Text + txtApellidos.Text, cboCurso.SelectedItem, lblAño.Text)
            If sms <> "" Then
                MessageBox.Show(sms)
            Else
                'cargarCombo()
                limpiarForm()
                cboCurso.SelectedIndex = -1
            End If

        End If

    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
End Class