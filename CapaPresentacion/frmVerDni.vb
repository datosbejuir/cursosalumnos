﻿Imports Entidades, CapaNegocios
Public Class frmVerDni
    Private Sub btnVer_Click(sender As Object, e As EventArgs) Handles btnVer.Click
        lblNombre.Visible = True
        lblDNI.Visible = True
        lblCurso.Visible = True
        lblAnio.Visible = True
        gbDNI.Visible = True
        If txtDNI.Text <> "" AndAlso txtDNI.Text.Length = 9 Then
            If misDatos.alumnos.Count > 0 Then
                For i As Byte = 0 To misDatos.alumnos.Count - 1
                    If misDatos.alumnos(i).Dni.Contains(txtDNI.Text) Then
                        Dim yPoint As Integer = 35
                        Dim xPoint As Integer = 60

                        Dim lblName As New Label
                        lblName.Name = "NombreAp"
                        lblName.Text = misDatos.alumnos(i).Nombre
                        lblName.Size = New Size(50, 13)
                        lblName.Location = New Point(xPoint, yPoint)
                        gbDNI.Controls.Add(lblName)

                        Dim lblCurs As New Label
                        lblCurs.Name = "Curso"
                        lblCurs.Text = misDatos.alumnos(i).Curso
                        lblCurs.Size = New Size(60, 13)
                        lblName.Location = New Point(xPoint, yPoint)
                        gbDNI.Controls.Add(lblCurs)

                        Dim lblAnio As New Label
                        lblAnio.Name = "Anio"
                        lblAnio.Text = misDatos.alumnos(i).Año
                        lblAnio.Size = New Size(50, 13)
                        lblAnio.Location = New Point(xPoint, yPoint)
                        gbDNI.Controls.Add(lblAnio)

                        yPoint = +15

                    Else
                        MessageBox.Show("No hay alumnos con ese DNI", "Ver alumnos")

                    End If
                Next

            Else
                MessageBox.Show("No hay alumnos", "Ver alumnos")
            End If
        End If
    End Sub

    Private Sub frmVerDni_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class