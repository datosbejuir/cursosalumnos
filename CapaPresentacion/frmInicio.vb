﻿Imports CapaNegocios
Public Class frmInicio


    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'cuando detecte que el formulario se va ha cerrar si se a producido un cambio nos preguntará si queremos guardar
        misDatos.Grabar()
    End Sub
    Private Sub AñadirAlumnoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AñadirAlumnoToolStripMenuItem.Click
        frmAnadir.Show()
    End Sub

    Private Sub BorrarAlumnoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BorrarAlumnoToolStripMenuItem.Click
        frmBorrar.Show()
    End Sub

    Private Sub VerAlumnoDNIToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VerAlumnoDNIToolStripMenuItem.Click
        frmVerDni.Show()
    End Sub

    Private Sub VerAlumnoMATRICULAToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VerAlumnoMATRICULAToolStripMenuItem.Click
        frmVerMatricula.Show()
    End Sub

    Private Sub VerCursosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VerCursosToolStripMenuItem.Click
        frmVerCurso.Show()
    End Sub
End Class
