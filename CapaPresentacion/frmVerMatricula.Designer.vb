﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVerMatricula
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnVer = New System.Windows.Forms.Button()
        Me.lblMatricula = New System.Windows.Forms.Label()
        Me.txtN = New System.Windows.Forms.TextBox()
        Me.lblDni = New System.Windows.Forms.Label()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.lblCurso = New System.Windows.Forms.Label()
        Me.gbMatricula = New System.Windows.Forms.GroupBox()
        Me.SuspendLayout()
        '
        'btnVer
        '
        Me.btnVer.Location = New System.Drawing.Point(197, 38)
        Me.btnVer.Name = "btnVer"
        Me.btnVer.Size = New System.Drawing.Size(75, 23)
        Me.btnVer.TabIndex = 0
        Me.btnVer.Text = "Ver"
        Me.btnVer.UseVisualStyleBackColor = True
        '
        'lblMatricula
        '
        Me.lblMatricula.AutoSize = True
        Me.lblMatricula.Location = New System.Drawing.Point(32, 44)
        Me.lblMatricula.Name = "lblMatricula"
        Me.lblMatricula.Size = New System.Drawing.Size(50, 13)
        Me.lblMatricula.TabIndex = 1
        Me.lblMatricula.Text = "Matricula"
        '
        'txtN
        '
        Me.txtN.Location = New System.Drawing.Point(104, 41)
        Me.txtN.Name = "txtN"
        Me.txtN.Size = New System.Drawing.Size(76, 20)
        Me.txtN.TabIndex = 2
        '
        'lblDni
        '
        Me.lblDni.AutoSize = True
        Me.lblDni.Location = New System.Drawing.Point(32, 86)
        Me.lblDni.Name = "lblDni"
        Me.lblDni.Size = New System.Drawing.Size(26, 13)
        Me.lblDni.TabIndex = 3
        Me.lblDni.Text = "DNI"
        Me.lblDni.Visible = False
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(78, 86)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(44, 13)
        Me.lblNombre.TabIndex = 4
        Me.lblNombre.Text = "Nombre"
        Me.lblNombre.Visible = False
        '
        'lblCurso
        '
        Me.lblCurso.AutoSize = True
        Me.lblCurso.Location = New System.Drawing.Point(194, 86)
        Me.lblCurso.Name = "lblCurso"
        Me.lblCurso.Size = New System.Drawing.Size(34, 13)
        Me.lblCurso.TabIndex = 5
        Me.lblCurso.Text = "Curso"
        Me.lblCurso.Visible = False
        '
        'gbMatricula
        '
        Me.gbMatricula.Location = New System.Drawing.Point(13, 103)
        Me.gbMatricula.Name = "gbMatricula"
        Me.gbMatricula.Size = New System.Drawing.Size(258, 153)
        Me.gbMatricula.TabIndex = 6
        Me.gbMatricula.TabStop = False
        Me.gbMatricula.Visible = False
        '
        'frmVerMatricula
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.gbMatricula)
        Me.Controls.Add(Me.lblCurso)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.lblDni)
        Me.Controls.Add(Me.txtN)
        Me.Controls.Add(Me.lblMatricula)
        Me.Controls.Add(Me.btnVer)
        Me.Name = "frmVerMatricula"
        Me.Text = "Form5"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnVer As Button
    Friend WithEvents lblMatricula As Label
    Friend WithEvents txtN As TextBox
    Friend WithEvents lblDni As Label
    Friend WithEvents lblNombre As Label
    Friend WithEvents lblCurso As Label
    Friend WithEvents gbMatricula As GroupBox
End Class
