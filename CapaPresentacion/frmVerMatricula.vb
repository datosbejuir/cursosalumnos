﻿Imports Entidades, CapaNegocios
Public Class frmVerMatricula

    Private Sub btnVer_Click(sender As Object, e As EventArgs) Handles btnVer.Click
        lblNombre.Visible = True
        lblDni.Visible = True
        lblCurso.Visible = True
        lblNombre.Visible = True
        gbMatricula.Visible = True
        If txtN.Text <> "" AndAlso IsNumeric(txtN.Text) Then
            If misDatos.alumnos.Count > 0 Then
                For i As Byte = 0 To misDatos.alumnos.Count - 1
                    If misDatos.alumnos(i).Dni.Contains(txtN.Text) Then
                        Dim yPoint As Integer = 35
                        Dim xPoint As Integer = 60

                        Dim lblDniAl As New Label
                        lblDniAl.Name = "Dni"
                        lblDniAl.Text = misDatos.alumnos(i).Nombre
                        lblDniAl.Size = New Size(50, 13)
                        lblDniAl.Location = New Point(xPoint, yPoint)
                        gbMatricula.Controls.Add(lblDniAl)

                        Dim lblNombre As New Label
                        lblNombre.Name = "Nombre"
                        lblNombre.Text = misDatos.alumnos(i).Curso
                        lblNombre.Size = New Size(60, 13)
                        lblDniAl.Location = New Point(xPoint, yPoint)
                        gbMatricula.Controls.Add(lblNombre)

                        Dim lblCurso As New Label
                        lblCurso.Name = "Anio"
                        lblCurso.Text = misDatos.alumnos(i).Año
                        lblCurso.Size = New Size(50, 13)
                        lblCurso.Location = New Point(xPoint, yPoint)
                        gbMatricula.Controls.Add(lblCurso)

                        yPoint = +15

                    Else
                        MessageBox.Show("No hay alumnos con esa matricula", "Ver alumnos")

                    End If
                Next

            Else
                MessageBox.Show("No hay alumnos", "Ver alumnos")
            End If
        End If
    End Sub
End Class