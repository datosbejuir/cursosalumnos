﻿Imports CapaNegocios
Public Class frmBorrar

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If txtMatricula.Text <> "" Then
            Dim sms As String = misDatos.Borrar(txtMatricula.Text)
            If sms <> "" Then
                MessageBox.Show(sms)
            Else
                txtMatricula.Text = ""
            End If
        Else
            Me.ErrorProvider1.Clear()
            Me.ErrorProvider1.SetError(txtMatricula, "Introduca una matricula")
        End If

    End Sub
    Private Sub txtMatricula_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtMatricula.KeyPress
        Const NUM = "0123456789" & ControlChars.Back
        If Not NUM.Contains(e.KeyChar.ToString) Then
            e.KeyChar = ""
        End If
    End Sub
    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
End Class